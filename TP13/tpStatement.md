
# Enoncé :

Créez un nouveau playbook qui permettra d'exclure les paquets Java*.

# Commandes :

Après la réalisation du playbook (cf playbook.yml) :
	
`ansible-playbook playbook.yml -i hosts.ini `
