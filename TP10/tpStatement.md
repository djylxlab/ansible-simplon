
#Enoncé :

Créer des utilisateurs en boucles : 
- Créer un playbook play-users.yml qui créera 3 utilisateurs
- Ces 3 utilisateurs appartiennent au groupe users.


# Commandes :

Après la réalisation du playbook (cf play-users.yml) :
	
`ansible-playbook playbook.yml -i hosts.ini `
