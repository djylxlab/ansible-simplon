# Ansible-Simplon

Bienvenue sur le Repository dédié au projet de l'entreprise Simplon.

Ce lab permettra via des playbooks "Ansible", la création et le déploiement d'un site web et d'une base de donnée sur 2 autres VMs dans le Cloud (TP final)(Microsoft Azure)

Le projet est composé de plusieurs TP. Chaque TP à des objectifs bien particulier et permet, à la fin et une fois chacun réalisés, d'atteindre le TP final

## Infrastructure

L'infra doit être configurer de la façon suivante : 

-   Groupe de Ressource : OCC_ASD_JACQUES
-   Réseau              : Vnet_OCC_ASD_JACQUES --> 10.0.11.0/24
-   Sous-réseau         : Subnet_Vnet_OCC_ASD_JACQUES : 16 sous-réseau et le premier sear celui réservé à l'admin
-   Groupe de sécurité : NGS_OCC_ASD_JACQUES

## Groupe de sécurité

Le groupe de sécurité doit être appliqué à l'ensemble du réseau et doit être paramétré pour les connexions ssh afin d'utiliser MobaXterm

La régle entrante est la suivante : 
-   Source               : IP publique 
-   Ports                : Any
-   Destination          : Service tag
-   Etiquette de service : Virtual Network
-   Service              : SSH
-   Action               : Autoriser

## Composition de chaques dossiers

Chaque Dossier (TP01, TP02, TP03, etc...) sera composé comme suit :

- 1 fichier `tpStatement.md` : Dans ce fichier vous trouverez l'énoncé du TP et la commande à saisir pour lancer le script
- 1 fichier `resulTP` : Dans ce fichier sera inscrit les résultat du script ou de la commande
- 1 fichier `playbook.yml` ou `autreNom.yml` (en fonction des directives) : Il y sera inscrit le script faisant fonctionner le playbook
- 1 fichier `hosts.ini` : Dans ce fichier, il y est inscrit les hôtes. (L'inventaire)

Il est possible que certains autres fichiers apparraissent en fonciton des besoins du TP.

## Création des VMs

Chaque VM sera créé manuellement sur Microsoft Azure

Composé de 3 Machines Virtuelles:

- 1 Debian : Ansible Master.
- 1 Debian : Node-web
- 1 Debian : Node-db

La VM Ansible-Master, peut se connecter en SSH, via échange de clés, aux 2 autres VMs

## Installation d'Ansible

Sur notre Master : 


```sh
$ UBUNTU_CODENAME=jammy
$ wget -O- "https://keyserver.ubuntu.com/pks/lookup?fingerprint=on&op=get&search=0x6125E2A8C77F2818FB7BD15B93C4A3FD7BB9C367" | sudo gpg --dearmour -o /usr/share/keyrings/ansible-archive-keyring.gpg
$ echo "deb [signed-by=/usr/share/keyrings/ansible-archive-keyring.gpg] http://ppa.launchpad.net/ansible/ansible/ubuntu $UBUNTU_CODENAME main" | sudo tee /etc/apt/sources.list.d/ansible.list
$ sudo apt update && sudo apt install ansible
```


