
# Enoncé du tp : 

Affichez l'espace disponible sur chaque noeud dans l'inventaire pui avec la commande debug vérifié si une variable  spécifique est présente dans l'inventory

# Commande : 

### Permet de resortir l'état des disques de chaque node

`ansible all -m shell -a "df -h" -i hosts`

### Permet de vérifier si la variable "ansible_ssh_host" est bien existante dans le fichier hosts

`ansible all -m debug -a "var='ansible_ssh_host'" -i hosts`
