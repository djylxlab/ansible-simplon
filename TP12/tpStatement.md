
# Enoncé :

Une fois l'installation effectué,
Utilisez le playbook pour modifier la page index.html

# Commandes :

Après la réalisation du playbook (cf playbook.yml) :
	
`ansible-playbook playbook.yml -i hosts.ini `
