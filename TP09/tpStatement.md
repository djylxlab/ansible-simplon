
# Enoncé :

Utilisation d'une variable locale de type dictionnaire.
Utiliser cette variable dans un message : 
"Le logiciel Apache sera installé par le paquet Httpd"


# Commandes :

Après la réalisation du playbook (cf playbook.yml) :
	
`ansible-playbook playbook.yml -i hosts.ini `
